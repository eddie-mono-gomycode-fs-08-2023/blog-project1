var mongoose = require('mongoose');

const loginSchema = new mongoose.Schema({    

  email: {
    type: String,
    required: true
  },

  password: {
    type: String,
    minLength: 6,
    required: true
  }
})

const Login = mongoose.model('login', loginSchema);
module.exports = Login;