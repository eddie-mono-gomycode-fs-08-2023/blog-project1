import React, {useState, useEffect} from 'react';
import axios from 'axios'

//https://animalfoundation.com/

const Blogs = () => {
  const [blogs, setBlogs] = useState([])
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [age, setAge] = useState('')
  const [password, setPassword] = useState('')

  useEffect(() => {
    axios.get('http://localhost:3300/blogs')
    .then((response) => {
      setBlogs(response.data)
    })
}, []);

  return (
  
<div>
{blogs.map((blog) => {
  return(
    <>
    <h1>{blog.name}</h1>
<h1>{blog.posts}</h1>
</>
  )
})}  
</div>
    
    
  );
}


export default Blogs;

