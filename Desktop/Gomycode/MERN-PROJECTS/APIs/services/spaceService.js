const spaceModel = require('../models/spaceModel')

const index = async (res)=>{

  try {
    const data = await spaceModel.find();
    return data;
  } catch (error) {
    res.status(error.status).json({error: error.message});

}

}


const find = async (id, res)=>{

  try {
    const data = await spaceModel.findById(id);
    return data;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const store = async (body, res)=>{

  try {

    const spaceSchema = new spaceModel(body);
    await spaceSchema.save();
    return spaceSchema;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const update = async (id, body, res)=>{

  try {
    await spaceModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const destroy = async (id, res)=>{

  try {
    await spaceModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

module.exports= {index, find, store, update, destroy}