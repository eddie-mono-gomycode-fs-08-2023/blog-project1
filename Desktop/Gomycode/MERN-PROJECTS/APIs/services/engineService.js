const engineModel = require('../models/engineModel');

const index = async ()=>{

  try {
    const data = await engineModel.find();
    return data;
  } catch (error) {
return error
  }

}

const find = async (id)=>{

  try {
    const data = await engineModel.findById(id);
    return data;
  } catch (error) {
return error
  }

}


const store = async (body)=>{

  try {

    const engineSchema = new engineModel(body);
    await engineSchema.save();
    return engineSchema;
  } catch (error) {
    return error
  }

}

const update = async (id, body)=>{

  try {
    await engineModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
    return error
  }

}

const destroy = async (id)=>{

  try {
    await engineModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    return error
  }

}

module.exports= {index, find, store, update, destroy}