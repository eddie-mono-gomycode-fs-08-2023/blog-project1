const mongoose = require('mongoose');
const {Schema} = mongoose

const engineSchema = new mongoose.Schema({
description: {
  type: String,
},
  photo: {
    type: String,
    require: false
  },
details: {
type: String,
},
user_id:{
  type: Schema.ObjectId, ref: 'User',
  required: false,
},

type_id:{
  type: Schema.ObjectId, ref: 'Type',
  required: false,

},
});

const Engine = mongoose.model('Engine', engineSchema);
module.exports = Engine;
