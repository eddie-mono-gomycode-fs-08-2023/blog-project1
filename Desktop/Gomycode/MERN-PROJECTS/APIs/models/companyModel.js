const mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
  name:{
    type: String,
  },
  address: {
    type: String,
  },
  photo: {
    type: String,
  },
  telephone: {
    type: Number,
  },
  email: {
    type: String,
  },
},
{
  timestamps: true
} 
);

const companyModel = mongoose.model('Company', companySchema);
module.exports = companyModel; 


