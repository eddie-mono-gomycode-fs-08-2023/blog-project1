const mongoose = require('mongoose');
const {Schema} = mongoose

const userSchema = new mongoose.Schema({
name: {
  type: String,
},
surname: {
  types: String,
},
telephone: {
  type: Number,
},
photo: {
  type: String,
  require: false
},
account_id:{
  type: Schema.ObjectId, ref: 'Account'},
});

const usersModel = mongoose.model('User', userSchema);
module.exports = usersModel;