const blogPostModel = require('../models/blogPostModel');

const index = async ()=>{

  try {
    const data = await blogPostModel.find();
    return data;
  } catch (error) {
return error
  }

}

const find = async (id)=>{

  try {
    const data = await blogPostModel.findById(id);
    return data;
  } catch (error) {
return error
  }

}

const store = async (body)=>{

  try {

    const blogPostSchema = new blogPostModel(body);
    await blogPostSchema.save();
    return blogPostSchema;
  } catch (error) {
return error
  }

}

const update = async (id, body)=>{

  try {
    await blogPostModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
return error
  }

}

const destroy = async (id, res)=>{

  try {
    await blogPostModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

module.exports= {index, find, store, update, destroy}