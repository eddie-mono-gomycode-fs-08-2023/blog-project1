import React, { useEffect, useState } from 'react';
import axios from 'axios';


const Dogs = () => {
  const [dogs, setDogs] = useState([])
  const [name, setName] = useState('')
  const [posts, setPosts] = useState('')

  useEffect(() => {
    axios.get('http://localhost:3300/dogs')
    .then((response) => {
      setDogs(response.data)
    })
  }, [])

  return (
    <div>
      {
        dogs.map((dog) => {
          return(
            <>
            <h1>{dog.name}</h1>
            <h3>{dog.posts}</h3>
            </>
          )
        })
      }
    </div>
  );
}

export default Dogs;
