import './App.css';
import Home from './Home';
import Navba from './Navba';
import Blogs from './Blogs';
import Signup from './Signup'
import Login from './Login';
import PostBirds from './Post-birds';
import PostOptions from './Post-options';
import Birds from './Birds';
import Dogs from './Dogs';
import { Routes, Route} from 'react-router-dom';
import Cats from './Cats';



function App() {
  return (
    <div className="App">
    <Navba/>
    {/* <Home/> */}

    <Routes>
        <Route index element={<Home />} />
        <Route path='blogs' element={<Blogs />} />
        <Route exact path='signup' element={<Signup />} />
        <Route path='postbirds' element={<PostBirds />} />
        <Route path='login' element={<Login />} />
        <Route path='postoptions' element={<PostOptions />} />
        <Route path='birds' element={<Birds />} />
        <Route path='dogs' element={<Dogs />} />
        <Route path='cats' element={<Cats />} />







      </Routes>
      
    </div>
  );
}

export default App;
