import axios from 'axios';
import React, { useEffect, useState } from 'react';

const Birds = () => {
  const [birds, setBirds] = useState([])
  const [name, setName] = useState('')
  const [posts, setPosts] = useState('')

  useEffect(() => {
    axios.get('http://localhost:3300/birds')
    .then((response) => {
      setBirds(response.data)
    })
}, []);
  return (
    <div>
      {birds.map((bird) => {
        return (
          <>
          <h1>{bird.name}</h1>
          <h3>{bird.posts}</h3>
          </>  
          
        )
      })}
      
    </div>
  );
}

export default Birds;
