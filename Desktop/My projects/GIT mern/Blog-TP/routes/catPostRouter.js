const express = require('express');
var router = express.Router();
const catpostController = require('../controllers/catPostController')


router.get('/', catpostController.index);
router.post('/', catpostController.store);
router.get('/:id', catpostController.find)
router.post('/', catpostController.store);
router.put('/:id', catpostController.update);
router.delete('/:id', catpostController.destroy);

module.exports = router;