import React, { useState } from 'react';
import axios from 'axios';
import {useNavigate } from 'react-router-dom';

const Login = () => {
  const [user, setUser] = useState([])
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const Navigate = useNavigate()

  const signIn = (e) => {
    e.preventDefault()
    axios.get('http://localhost:3300/signup', {email, password})
    .then(() => {
      setUser({email, password})
      if(user) {
        Navigate('/postoptions')
      }else {
        Navigate('/signup')

      }
    })
setEmail('')
setPassword('')

    }


  return (
    <div>
    <>
      <form>
        <label>
        Name
        <input type='text' name='email' value={email} placeholder='Enter Email' onChange={(e) => setEmail(e.target.value)}/>
        </label>

        <label>
        Password
        <input type='password' name='password' value={password} placeholder='Enter Password' onChange={(e) => setPassword(e.target.value)}/>
        <button onClick={(signIn)}>LOGIN</button>
        </label>

      </form>
    </>
      
    </div>
  );
}

export default Login;

