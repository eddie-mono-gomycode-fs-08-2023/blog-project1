const dogPostModel = require('../models/dogPostModel');

const index = async ()=>{

  try {
    const data = await dogPostModel.find();
    return data;
  } catch (error) {
return error
  }

}

const find = async (id)=>{

  try {
    const data = await dogPostModel.findById(id);
    return data;
  } catch (error) {
return error
  }

}

const store = async (body)=>{

  try {

    const dogPostSchema = new dogPostModel(body);
    await dogPostSchema.save();
    return dogPostSchema;
  } catch (error) {
return error
  }

}

const update = async (id, body)=>{

  try {
    await dogPostModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
return error
  }

}

const destroy = async (id, res)=>{

  try {
    await dogPostModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

module.exports= {index, find, store, update, destroy}